## Nextcloud in docker using jwilder nginx-proxy
[Main documentation](https://github.com/nextcloud/docker)

[I use this VPS hosting](https://www.hostens.com/?affid=1251)

### Requirements
1. This installation is relay on configured [jwilder/nginx-proxy](https://github.com/nginx-proxy/nginx-proxy).

    I use this installation of [nginx-proxy](https://gitlab.com/sync-server/nginx-proxy)

1. Domain which points to your server.

### CI/CD
If you are going to use GitLab CI/CD to deploy this `docker-compose.yml`
you need to setup variables in `your gitlab project - Settings - CI/CD - Variables`:
- `SERVER_KEY` - SSH private key to access your server. Server should have ssh public key in `.ssh/authorized_keys` file.
- `SERVER_PORT` - SSH port of server
- `SERVER_USER` - servers user
- `SERVER_HOST` - IP or domain of the server

and variables from `docker-compose.yml`

- `MYSQL_ROOT_PASS` - some random strong password
- `MYSQL_PASS` - some random strong password
- `BASE_DOMAIN` - your domain

### Variables
If you just want to use `docker-compose.yml` without CI/CD, place last three variables in `.env` file near your `docker-compose.yml`.

### Update nextcloud version
- `docker-compose pull` - to pull new images
- `docker-compose up -d` - to up it with new images
- `docker system prune` - optional, to delete old (untagget) images

**It is only possible to upgrade one major version at a time. For example, if you want to upgrade from version 14 to 16, you will have to upgrade from version 14 to 15, then from 15 to 16.**

### Note
This installation can't send emails (e.g. in case you forgot your password).
To send emails you need to setup SMP server and configure your nextloud instance according to official documentation.

### 